<?php

namespace App\Controller;

use App\Entity\Job;
use App\Form\JobType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class JobController.
 *
 * @package App\Controller
 */
class JobController extends AbstractController
{
    const LOGOS_SUBDIRECTORY = 'logos';

    /**
     * Job view page controller.
     *
     * @param Job $job
     *   Job to display.
     *
     * @return Response
     *   HTTP response object.
     */
    public function view(Job $job)
    {
        return $this->render(
            'job/view.html.twig',
            [
                'controller_name' => 'JobeetController',
                'job' => $job,
            ]
        );
    }

    /**
     * Add job page.
     *
     * @param Request $request
     *   HTTP request.
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     *
     * @return RedirectResponse|Response
     *   HTTP response.
     */
    public function add(Request $request, EntityManagerInterface $entityManager)
    {
        $job = new Job();

        $form = $this->form($request, $entityManager, $job);

        if ($form instanceof RedirectResponse) {
            return $form;
        }

        return $this->render(
            'job/add.html.twig',
            [
                'controller_name' => 'JobController',
                'form' => $form->createView(),
                'job' => $job,
            ]
        );
    }

    /**
     * Job edit page.
     *
     * @param Request $request
     *   HTTP request object.
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Job $job
     *   Job to edit.
     *
     * @return Response|RedirectResponse
     *   HTTP response object.
     */
    public function edit(Request $request, EntityManagerInterface $entityManager, Job $job)
    {
        $form = $this->form($request, $entityManager, $job);

        if ($form instanceof RedirectResponse) {
            return $form;
        }

        return $this->render(
            'job/edit.html.twig',
            [
                'controller_name' => 'JobController',
                'form' => $form->createView(),
                'job' => $job,
            ]
        );
    }

    /**
     * Job delete page.
     *
     * @param Request $request
     *   HTTP request object.
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Job $job
     *   Job to edit.
     *
     * @return Response|RedirectResponse
     *   HTTP response object.
     */
    public function delete(Request $request, EntityManagerInterface $entityManager, Job $job)
    {
        $form = $this->createFormBuilder($job)
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'Delete job',
                ]
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $position = $job->getPosition();
            $entityManager->remove($job);
            $entityManager->flush();

            $this->addFlash('success', "The job \"$position\" has been deleted.");
            $this->redirectToRoute('index');

        }

        return $this->render(
            'job/delete.html.twig',
            [
                'controller_name' => 'JobController',
                'form' => $form->createView(),
                'job' => $job,
            ]
        );
    }

    /**
     * Form used in add and edit job page.
     *
     * @param Request $request
     *   HTTP request object.
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Job $job
     *   Job to edit.
     *
     * @return FormInterface
     *   Job form object.
     */
    protected function form(Request $request, EntityManagerInterface $entityManager, Job $job)
    {
        $form = $this->createForm(JobType::class, $job);

        $form->handleRequest($request);
        $error = false;
        if ($request->isXmlHttpRequest()) {
            $logo = $request->request->get('logo');
            $job->setLogo($logo);
        } elseif ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $logoFile */
            $logoFile = $form->get('logo')->getData();

            // this condition is needed because the 'logo' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($logoFile) {
                $originalFilename = pathinfo($logoFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate(
                    'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
                    $originalFilename
                );
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logoFile->guessExtension();
                $currentDate = new \DateTime();
                $logoSubDir = '/logos/'.$currentDate->format('\mm');
                // Move the file to the directory where logos are stored
                try {


                    $logoFile->move(
                        $this->getParameter('public_files_directory').$logoSubDir,
                        $newFilename
                    );
                    $job->setLogo($logoSubDir.'/'.$newFilename);

                } catch (FileException $e) {
                    $this->addFlash('error', 'Error during logo upload.');
                    $error = true;
                }
            } else {
                $job->setLogo(null);
            }

            if (!$error) {
                // updates the 'logoFilename' property to store the PDF file name
                // instead of its contents

                $entityManager->persist($job);
                $entityManager->flush();

                return $this->redirectToRoute('job_view', ['id' => $job->getId()]);
            }
        }

        return $form;
    }


}
