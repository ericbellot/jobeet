<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Job;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CategoryController.
 *
 * @package App\Controller
 */
class CategoryController extends AbstractController
{
    /**
     * Number of jobs displayed in category page.
     */
    const JOBS_NUMBER_BY_CATEGORY_PAGE = 3;

    /**
     * List all categories of Jobeet.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request.
     *
     * @return RedirectResponse|Response
     *   Redirect response object.
     */
    public function list(EntityManagerInterface $entityManager, Request $request)
    {
        $categories = $entityManager->getRepository(Category::class)->findAll();

        $category = new Category();

        $form = $this->categoryForm($entityManager, $request, $category);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('category_list');
        }

        return $this->render(
            'category/list.html.twig',
            [
                'controller_name' => 'CategoryController',
                'categories' => $categories,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Category view page.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request object.
     * @param Category $category
     *   Category to display.
     *
     * @return Response|RedirectResponse
     *   HTTP response object.
     */
    public function view(EntityManagerInterface $entityManager, Request $request, Category $category)
    {
        // Get jobs.
        $jobs = $entityManager->getRepository(Job::class)->findBy(
            ['category' => $category],
            ['id' => 'ASC']
        );

        // Get page number (pagination).
        $page = filter_var($request->query->get('page'), FILTER_VALIDATE_INT, ['min_range' => 1]);
        if (!$page) {
            $page = 1;
        }

        // Select jobs displayed in current page.
        $start = ($page - 1) * self::JOBS_NUMBER_BY_CATEGORY_PAGE;
        $pageJobs = array_slice($jobs, $start, self::JOBS_NUMBER_BY_CATEGORY_PAGE);

        if (!$pageJobs && $page > 1) {
            throw $this->createNotFoundException('Wrong category page.');
        }

        $totalJobs = count($jobs);

        $nextPage = null;
        if ($pageJobs) {
            $nextPage = end($pageJobs)->getid() !== end($jobs)->getid() ? $page + 1 : null;
        }

        $lastPage = 1;
        if ($totalJobs) {
            $lastPage = ceil($totalJobs / self::JOBS_NUMBER_BY_CATEGORY_PAGE);
        }

        $render = $this->render(
            'category/view.html.twig',
            [
                'controller_name' => 'CategoryController',
                'jobs' => $pageJobs,
                'jobs_total_number' => $totalJobs,
                'category' => $category,
                'current_page' => $page,
                'previous_page' => $page > 1 ? $page - 1 : null,
                'next_page' => $nextPage,
                'last_page' => $lastPage,
            ]
        );

        return $render;
    }

    /**
     * Category edit page.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request object.
     * @param Category $category
     *   Category to edit.
     *
     * @return Response|RedirectResponse
     *   HTTP response object.
     */
    public function edit(EntityManagerInterface $entityManager, Request $request, Category $category)
    {
        $form = $this->categoryForm($entityManager, $request, $category);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('category_list');
        }

        return $this->render(
            'category/edit.html.twig',
            [
                'controller_name' => 'CategoryController',
                'form' => $form->createView(),
                'category' => $category,
            ]
        );
    }

    /**
     * Category view page controller.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Category $category
     *   Category to delete.
     *
     * @return RedirectResponse
     *   HTTP response object.
     */
    public function delete(EntityManagerInterface $entityManager, Category $category)
    {
        $name = $category->getName();
        $entityManager->remove($category);
        $entityManager->flush();
        $this->addFlash('success', "The category  \"$name\" has been deleted.");

        return $this->redirectToRoute('category_list');
    }

    /**
     * Category form used in add and edit page.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request object.
     * @param Category $category
     *   Category to display.
     *
     * @return FormInterface
     *   Category form object.
     */
    protected function categoryForm(EntityManagerInterface $entityManager, Request $request, Category $category)
    {
        $form = $this->createFormBuilder($category)
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'Name',
                    'required' => true,
                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => $category->getId() ? 'Save' : 'Add',
                ]
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                /** @var Category $category */
                $category = $form->getData();
                $name = $category->getName();
                $entityManager->persist($category);
                $entityManager->flush();
                $this->addFlash('success', "Category \"$name\" has been created successfully.");
            } catch (NotNullConstraintViolationException $e) {
                $this->addFlash('error', "A category must have a name.");
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('error', "The category \"$name\" exists yet. Operation stopped.");
            }
        }

        return $form;
    }

}
