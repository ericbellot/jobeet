<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Job;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class JobeetController.
 *
 * @package App\Controller
 */
class JobeetController extends AbstractController
{
    /**
     * Number of jobs by category in home page.
     */
    const JOBS_NUMBER_BY_HOME_CATEGORY = 3;

    /**
     * Home page controller.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     *
     * @return RedirectResponse|Response
     *   HTTP response object.
     */
    public function index(EntityManagerInterface $entityManager)
    {
        /** @var Category[] $categories */
        $categories = $entityManager->getRepository(Category::class)->findNotEmpty();

        $categorizedJobs = [];
        foreach ($categories as $category) {
            $jobs = $entityManager->getRepository(Job::class)->findBy(
                [
                    'category' => $category->getId(),
                    'public' => true,
                ],
                ['id' => 'ASC'],
                4
            );
            if (!$jobs) {
                continue;
            }
            $hasMoreLink = false;
            if (count($jobs) > 3) {
                $hasMoreLink = true;
                array_pop($jobs);
            }
            $categorizedJobs[] = [
                'category' => $category,
                'jobs' => $jobs,
                'more_link' => $hasMoreLink,
            ];
        }

        return $this->render(
            'jobeet/index.html.twig',
            [
                'controller_name' => 'JobeetController',
                'categorized_jobs' => $categorizedJobs,
            ]
        );
    }

    /**
     * Search results page controller.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request object.
     *
     * @return Response|RedirectResponse
     *   HTTP response object.
     */
    public function searchResultsView(EntityManagerInterface $entityManager, Request $request)
    {
        $terms = $request->query->get('form')['search'] ?? null;
        $cleanTerms = [];
        if ($terms) {
            $cleanTerms = explode(' ', transliterator_transliterate('Any-Latin; Latin-ASCII; Lower()', $terms));
        }

        if ($cleanTerms) {
            $publicJobs = $entityManager->getRepository(Job::class)->findBy(['public' => true]);
            $jobs = [];
            foreach ($publicJobs as $job) {
                $fulltext = transliterator_transliterate(
                    'Any-Latin; Latin-ASCII; Lower()',
                    implode(
                        ' ',
                        [
                            $job->getPosition(),
                            $job->getLocation(),
                            $job->getMember()->getCompany(),
                            $job->getCategory()->getName(),
                        ]
                    )
                );
                foreach ($cleanTerms as $cleanTerm) {
                    if (strpos($fulltext, $cleanTerm) !== false) {
                        $jobs[] = $job;
                        continue 2;
                    }
                }
            }
        } else {
            $jobs = $entityManager->getRepository(Job::class)->findAll();
        }

        return $this->render(
            'jobeet/search_results_view.html.twig',
            [
                'controller_name' => 'JobeetController',
                'jobs' => $jobs,
            ]
        );
    }

    /**
     * Search form (used in all pages of site).
     *
     * @return Response
     *   HTTP response object.
     *
     * @see /templates/base.html.twig
     */
    public function searchForm()
    {
        $form = $this->createFormBuilder()
            ->add(
                'search',
                SearchType::class,
                [
                    'label' => 'Search',
                    'attr' => [
                        'placeholder' => 'Type your search',
                    ],
                    'constraints' => [
                        new Regex(
                            [
                                'pattern' => '/^ *[\w-]+( +[\w-]+)* *$/',
                                'message' => 'Search terms must only contain letters, numbers and dashes.',
                            ]
                        ),
                    ],
                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'Search',
                ]
            )
            ->setAction($this->generateUrl('search_results_view'))
            ->setMethod('GET')
            ->getForm();


        return $this->render(
            'jobeet/search_form.html.twig',
            [
                'controller_name' => 'JobeetController',
                'search_form' => $form->createView(),
            ]
        );
    }

}
