<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Job;
use App\Entity\Member;
use App\Form\MemberType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MemberController.
 *
 * @package App\Controller
 */
class MemberController extends AbstractController
{
    /**
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request.
     *
     * @return Response
     *   Redirect response object.
     */
    public function list(EntityManagerInterface $entityManager, Request $request)
    {
        $members = $entityManager->getRepository(Member::class)->findAll();

        return $this->render(
            'member/list.html.twig',
            [
                'controller_name' => 'MemberController',
                'members' => $members,
            ]
        );
    }

    /**
     * Member view page.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request object.
     * @param Member $member
     *   Memebr to display.
     *
     * @return Response|RedirectResponse
     *   HTTP response object.
     */
    public function view(Request $request, EntityManagerInterface $entityManager, Member $member)
    {
        $categoryForm = $this->createFormBuilder()
            ->add(
                'category',
                EntityType::class,
                [
                    'label' => 'Select a category:',
                    'class' => Category::class,
                    'choice_label' => 'name',
                    'multiple' => false,
                    'expanded' => false,
                    'required' => false,
                    'placeholder' => 'All categories',
                    // Show only categories used in public jobs belonging to current member.
                    'query_builder' => function (EntityRepository $er) use ($member) {
                        return $er->createQueryBuilder('c')
                            ->setParameter('member_id', $member->getId())
                            ->innerJoin(
                                'App\\Entity\\Job',
                                'j',
                                \Doctrine\ORM\Query\Expr\Join::WITH,
                                'c.id = j.category AND j.member = :member_id AND j.public = 1'
                            )
                            ->orderBy('c.name', 'ASC');
                    },

                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'Select',
                ]
            )
            ->getForm();

        $categoryForm->handleRequest($request);

        $category = null;
        if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {
            $category = $categoryForm->getData()['category'] ?? null;
        }

        $criteria = [
            'public' => true,
            'member' => $member->getId(),
        ];
        if ($category instanceof Category) {
            $criteria['category'] = $category->getId();
        }
        $jobs = $entityManager->getRepository(Job::class)->findBy($criteria);

        return $this->render(
            'member/view.html.twig',
            [
                'controller_name' => 'MemberController',
                'member' => $member,
                'jobs' => $jobs,
                'category_form' => $categoryForm->createView(),
            ]
        );
    }

    /**
     * Member add page.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request object.
     *
     * @return Response|RedirectResponse
     *   HTTP response object.
     */
    public function add(Request $request, EntityManagerInterface $entityManager)
    {
        $member = new Member();

        $form = $this->form($request, $entityManager, $member);

        if ($form instanceof RedirectResponse) {
            return $form;
        }

        return $this->render(
            'member/add.html.twig',
            [
                'controller_name' => 'MemberController',
                'form' => $form->createView(),
                'member' => $member,
            ]
        );
    }

    /**
     * /**
     * Member edit page.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request object.
     * @param Member $member
     *   Memebr to edit.
     *
     * @return Response|RedirectResponse
     *   HTTP response object.
     */
    public function edit(Request $request, EntityManagerInterface $entityManager, Member $member)
    {
        $form = $this->form($request, $entityManager, $member);

        if ($form instanceof RedirectResponse) {
            return $form;
        }

        return $this->render(
            'member/edit.html.twig',
            [
                'controller_name' => 'MemberController',
                'form' => $form->createView(),
                'member' => $member,
            ]
        );
    }

    /**
     * /**
     * Member delete page.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request object.
     * @param Member $member
     *   Memebr to delete.
     *
     * @return Response|RedirectResponse
     *   HTTP response object.
     */
    public function delete(Request $request, EntityManagerInterface $entityManager, Member $member)
    {
        $form = $this->createFormBuilder()
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'Delete member',
                ]
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company = $member->getCompany();
            $entityManager->remove($member);
            $entityManager->flush();

            $this->addFlash('success', "The affiliate member \"$company\" and all her jobs has been deleted.");

            return $this->redirectToRoute('member_list');
        }

        return $this->render(
            'member/delete.html.twig',
            [
                'controller_name' => 'JobController',
                'form' => $form->createView(),
                'member' => $member,
            ]
        );
    }

    /**
     * /**
     * Form used in add and edit member pages.
     *
     * @param EntityManagerInterface $entityManager
     *   Entity Manager.
     * @param Request $request
     *   HTTP request object.
     * @param Member $member
     *   Member to display.
     *
     * @return FormInterface
     *   Member form object.
     */
    protected function form(Request $request, EntityManagerInterface $entityManager, Member $member)
    {
        $form = $this->createForm(MemberType::class, $member);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($member);
            $entityManager->flush();

            return $this->redirectToRoute('member_view', ['id' => $member->getId()]);
        }

        return $form;
    }
}
