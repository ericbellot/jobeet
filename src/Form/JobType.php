<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Job;
use App\Entity\Member;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class JobType.
 *
 * @package App\Form
 */
class JobType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'member',
                EntityType::class,
                [
                    'label' => 'Affiliate member',
                    'class' => Member::class,
                    'choice_label' => 'company',
                    'multiple' => false,
                    'expanded' => false,

                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label' => 'Type',
                    'choices' => [
                        'Full time' => 'fulltime',
                        'Part time' => 'parttime',
                        'Freelance' => 'freelance',
                    ],
                    'data' => 'fulltime',
                    'expanded' => true,
                    'required' => true,
                ]
            )
            ->add(
                'logo',
                FileType::class,
                [
                    'label' => 'Logo de l\'entreprise',
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'position',
                TextType::class,
                [
                    'label' => 'Position',
                    'attr' => [
                        'placeholder' => 'Job position',
                    ],
                ]
            )
            ->add(
                'location',
                TextType::class,
                [
                    'label' => 'Location',
                    'attr' => [
                        'placeholder' => 'Job location',
                    ],
                ]
            )
            ->add(
                'category',
                EntityType::class,
                [
                    'label' => 'Category',
                    'class' => Category::class,
                    'choice_label' => 'name',
                    'multiple' => false,
                    'expanded' => false,

                ]
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'label' => 'Job description',
                ]
            )
            ->add(
                'public',
                CheckboxType::class,
                [
                    'label' => 'Public',
                    'required' => false,

                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'Enregistrer',
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Job::class,
            ]
        );
    }
}
