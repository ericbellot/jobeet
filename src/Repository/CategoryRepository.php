<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Member;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    /**
     * {@inheritDoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * Find categories used by jobs.
     *
     * @return int|mixed|string
     *   Result of query.
     */
    public function findNotEmpty()
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('App\\Entity\\Job', 'j', \Doctrine\ORM\Query\Expr\Join::WITH, 'c.id = j.category')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

}
