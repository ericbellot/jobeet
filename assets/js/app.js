/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/bootstrap.scss';
import '../css/base.scss';
import '../css/layout.scss';
import '../css/modules.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';
// Enable jQuery.
const $ = require('jquery');
// create global $ and jQuery variables
global.$ = global.jQuery = $;


/**
 * Fix display of filename in Booststrap File widget.
 */
$('.custom-file-input').each(function () {
    $(this).change(function (event) {
        var fileName = event.target.files[0].name;
        var nextSibling = event.target.nextElementSibling;
        nextSibling.innerText = fileName;
    });
});

$('#job_logo').change(function (event) {
    var file = event.target.files[0];
    var $form = $(this).closest('form');
    var data = {};
    data['logo'] = file;
    $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: data,
        success: function (html) {
            $('#image-logo-wrapper').replaceWith(
                $(html).find('#image-logo-wrapper')
            );

        }
    });
});


$('#remove-image-logo').click(function () {
    var $form = $(this).closest('form');
    var data = {};
    data['logo'] = null;
    $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: data,
        success: function (html) {
            $('#image-logo-wrapper').replaceWith(
                $(html).find('#image-logo-wrapper')
            );

        }
    });
});
