# TD Jobeet

Ce site Web est un exercice pour la formation Cegefos - Symfony.

## Environnement

Le site a été réalisé dans l'environnement LAMP suivant :

- Debian Buster
- Apache 2.4
- PHP 7.3
- MariaBD 10.4

Les logiciels suivants sont installés :

- Composer 1.10
- Git 2.20
- Phive 0.13
- Symfony 4.14
- Yarn 1.22

L'environnement de développement ci-desuss a été réalisé avec [Drupal 8 docker for developer](https://gitlab.com/ericbellot/d8-docker-dev) ; un environnement Docker pour le développement de Drupal 8, qui fonctionne aussi très bien pour Symfony.

## Installation

Pour installer le site:

~~~ {.bash}
git clone git@gitlab.com:ericbellot/jobeet.git jobeet
cd jobeet
composer install
~~~


